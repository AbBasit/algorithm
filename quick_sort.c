//A quick_sort program 
#include<stdio.h>
#define MAX 10

void swap(int *x, int *y){
  
  int tmp;
  tmp = *x;
  *x = *y;
  *y = tmp;
}

int getkeyposition(int i, int j){
  
  return ((i + j) / 2);
  
}

void quicksort(int list[], int m, int n){

  int key, i, j, k;

  if(m < n){
    k = getkeyposition(m, n);
    swap( &list[m], &list[k]);    
    key = list[m];
    i = m + 1;
    j = n;
    while(i <= j){ 
      while((i<= n) && (list[i] <= key))
	i++;
      
      while((j >= m) && (list[j] > key))	
	j--;
      if(i < j)
	swap(&list[i], &list[j]);
    }
   
    swap(&list[m], &list[j]);
    quicksort(list, m, j - 1);
    quicksort(list, j + 1, n);
  }
}



void readlist(int list[], int n){
  
  int i;
  printf("Enter the elements\n");
   
  for(i = 0; i < n; i++){
    printf("%d>", i + 1);
    scanf("%d",&list[i]);
  }	   
}


void printlist(int list[], int n){
  int i;
  //printf("\nThe elements of the list are:\n");

  for(i = 0; i < n; i++)
    
    printf("%d\t", list[i]);
}

int main(){


  int list[MAX], n;
  
  printf("Enter Size of array? ");
  scanf("%d",&n);

  readlist( list, n);

  printf("\nThe list before sorting is:\n");
  
  printlist( list, n);
  
  quicksort( list, 0, n-1);
  
  printf("\nThe list after sorting is:\n");
  
  printlist( list, n);
  
  return 0;
}
